Wylie = {

    chars: {

    consonant: {
      k	: "\u0f40",
      kh	: "\u0f41",
      g	: "\u0f42",
      gh	: "\u0f42\u0fb7",
     "g+h"	: "\u0f42\u0fb7",
      ng	: "\u0f44",
      c	: "\u0f45",
      ch	: "\u0f46",
      j	: "\u0f47",
      ny	: "\u0f49",
      T	: "\u0f4a",
     "-t"	: "\u0f4a",
      Th	: "\u0f4b",
     "-th"	: "\u0f4b",
      D	: "\u0f4c",
     "-d"	: "\u0f4c",
      Dh	: "\u0f4c\u0fb7",
     "D+h"	: "\u0f4c\u0fb7",
     "-dh"	: "\u0f4c\u0fb7",
     "-d+h"	: "\u0f4c\u0fb7",
      N	: "\u0f4e",
     "-n"	: "\u0f4e",
      t	: "\u0f4f",
      th	: "\u0f50",
      d	: "\u0f51",
      dh	: "\u0f51\u0fb7",
     "d+h"	: "\u0f51\u0fb7",
      n	: "\u0f53",
      p	: "\u0f54",
      ph	: "\u0f55",
      b	: "\u0f56",
      bh	: "\u0f56\u0fb7",
     "b+h"	: "\u0f56\u0fb7",
      m	: "\u0f58",
      ts	: "\u0f59",
      tsh	: "\u0f5a",
      dz	: "\u0f5b",
      dzh	: "\u0f5b\u0fb7",
     "dz+h"	: "\u0f5b\u0fb7",
      w	: "\u0f5d",
      zh	: "\u0f5e",
      z	: "\u0f5f",
     "'"	: "\u0f60",
     "\u2018"	: "\u0f60", // typographic quotes
     "\u2019"	: "\u0f60",
      y	: "\u0f61",
      r	: "\u0f62",
      l	: "\u0f63",
      sh	: "\u0f64",
      Sh	: "\u0f65",
     "-sh"	: "\u0f65",
      s	: "\u0f66",
      h	: "\u0f67",
      W	: "\u0f5d",
      Y	: "\u0f61",
      R	: "\u0f6a",
      f	: "\u0f55\u0f39",
      v	: "\u0f56\u0f39",
  },

    subjoined: {
      k	: "\u0f90",
      kh	: "\u0f91",
      g	: "\u0f92",
      gh	: "\u0f92\u0fb7",
     "g+h"	: "\u0f92\u0fb7",
      ng	: "\u0f94",
      c	: "\u0f95",
      ch	: "\u0f96",
      j	: "\u0f97",
      ny	: "\u0f99",
      T	: "\u0f9a",
     "-t"	: "\u0f9a",
      Th	: "\u0f9b",
     "-th"	: "\u0f9b",
      D	: "\u0f9c",
     "-d"	: "\u0f9c",
      Dh	: "\u0f9c\u0fb7",
     "D+h"	: "\u0f9c\u0fb7",
     "-dh"	: "\u0f9c\u0fb7",
     "-d+h"	: "\u0f9c\u0fb7",
      N	: "\u0f9e",
     "-n"	: "\u0f9e",
      t	: "\u0f9f",
      th	: "\u0fa0",
      d	: "\u0fa1",
      dh	: "\u0fa1\u0fb7",
     "d+h"	: "\u0fa1\u0fb7",
      n	: "\u0fa3",
      p	: "\u0fa4",
      ph	: "\u0fa5",
      b	: "\u0fa6",
      bh	: "\u0fa6\u0fb7",
     "b+h"	: "\u0fa6\u0fb7",
      m	: "\u0fa8",
      ts	: "\u0fa9",
      tsh	: "\u0faa",
      dz	: "\u0fab",
      dzh	: "\u0fab\u0fb7",
     "dz+h"	: "\u0fab\u0fb7",
      w	: "\u0fad",
      zh	: "\u0fae",
      z	: "\u0faf",
     "'"	: "\u0fb0",
     "\u2018"	: "\u0fb0",	// typographic quotes
     "\u2019"	: "\u0fb0",
      y	: "\u0fb1",
      r	: "\u0fb2",
      l	: "\u0fb3",
      sh	: "\u0fb4",
      Sh	: "\u0fb5",
     "-sh"	: "\u0fb5",
      s	: "\u0fb6",
      h	: "\u0fb7",
      a	: "\u0fb8",
      W	: "\u0fba",
      Y	: "\u0fbb",
      R	: "\u0fbc",
  },

    vowel: {
      a	: "\u0f68",	// a-chen
      A	: "\u0f71",
      i	: "\u0f72",
      I	: "\u0f71\u0f72",
      u	: "\u0f74",
      U	: "\u0f71\u0f74",
      e	: "\u0f7a",
      ai	: "\u0f7b",
      o	: "\u0f7c",
      au	: "\u0f7d",
     "-i"	: "\u0f80",
     "-I"	: "\u0f71\u0f80",

     // special sanskrit vowels
     "r-i"	: "\u0fb2\u0f80",
     "r-I"	: "\u0fb2\u0f71\u0f80",
     "l-i"	: "\u0fb3\u0f80",
     "l-I"	: "\u0fb3\u0f71\u0f80",
 },

    // these vowels have different forms for the standalone thing and when added to a syllable
    // Wylie : [ 'standalone', 'added' ]
    complex_vowel: {
      "r-i"	: [ "\u0f62\u0f80",	 "\u0fb2\u0f80" ],
      "r-I"	: [ "\u0f62\u0f71\u0f80", "\u0fb2\u0f71\u0f80" ],
      "l-i"	: [ "\u0f63\u0f80",	 "\u0fb3\u0f80" ],
      "l-I"	: [ "\u0f63\u0f71\u0f80", "\u0fb3\u0f71\u0f80" ],
  },

    // stuff that can come after the vowel
    // symbol : [ unicode, class ]  (cannot have more than 1 of the same class in the same stack)
    final: {
      M	: [ "\u0f7e", "M" ],	// anusvara / bindu / circle above / nga ro
      H	: [ "\u0f7f", "H" ],	// visarga / rnam bcad
     "~M`"	: [ "\u0f82", "M" ],	// crescent, bindu & nada
     "~M"	: [ "\u0f83", "M" ],	// crescent & bindu
     "?"	: [ "\u0f84", "?" ],	// halanta / srog med
     "X"	: [ "\u0f37", "X" ],	// small circle under
     "~X"	: [ "\u0f35", "X" ],	// small circle w/ crescent under
     "^"	: [ "\u0f39", "^" ],	// tsa-phru
 },

    other: {
      0	: "\u0f20",
      1	: "\u0f21",
      2	: "\u0f22",
      3	: "\u0f23",
      4	: "\u0f24",
      5	: "\u0f25",
      6	: "\u0f26",
      7	: "\u0f27",
      8	: "\u0f28",
      9	: "\u0f29",
    // "-"	: "\u0f0b",
     "\n"   : "\n",
     " "	: "\u0f0b",
     "*"	: "\u0f0c",
     "/"	: "\u0f0d",
     "//"	: "\u0f0e",
     ";"	: "\u0f0f",
     "|"	: "\u0f11",
     "!"	: "\u0f08",
     ":"	: "\u0f14",
     "_"	: " ",
     "."    : "", // separates stacks but isn't output
     "="	: "\u0f34",
     "<"	: "\u0f3a",
     ">"	: "\u0f3b",
     "("	: "\u0f3c",
     ")"	: "\u0f3d",
     "@"	: "\u0f04",
     "#"	: "\u0f05",
     '$'	: "\u0f06",
     "%"	: "\u0f07",
 },
 },

    sets: {

    // special characters: flag those if they occur out of context
    special: {".":1, "+":1, "-":1, "~":1, "^":1, "?":1, "`":1, "]":1},

    // superscript : { letter or stack below : 1 }
    superscripts: {
      r	: { k:1, g:1, ng:1, j:1, ny:1, t:1, d:1, n:1, b:1, m:1, ts:1, dz:1,
          "k+y":1, "g+y":1, "m+y":1, "ts+w":1, "g+w":1 },
      l	: { k:1, g:1, ng:1, c:1, j:1, t:1, d:1, p:1, b:1, h:1 },
      s	: { k:1, g:1, ng:1, ny:1, t:1, d:1, n:1, p:1, b:1, m:1, ts:1,
          "k+y":1, "g+y":1, "p+y":1, "b+y":1, "m+y":1, "k+r":1, "g+r":1, "p+r":1, "b+r":1, "m+r":1, "n+r":1 },
  },

    // subscript : { letter or stack above : 1 }
    subscripts: {
      y	: { k:1, kh:1, g:1, p:1, ph:1, b:1, m:1,
          "r+k":1, "r+g":1, "r+m":1, "s+k":1, "s+g":1, "s+p":1, "s+b":1, "s+m":1 },
      r	: { k:1, kh:1, g:1, t:1, th:1, d:1, p:1, ph:1, b:1, m:1, sh:1, s:1, h:1,
          "s+k":1, "s+g":1, "s+p":1, "s+b":1, "s+m":1, "s+n":1 },
      l	: { k:1, g:1, b:1, r:1, s:1, z:1 },
      w	: { k:1, kh:1, g:1, c:1, ny:1, t:1, d:1, ts:1, tsh:1, zh:1, z:1, r:1, sh:1, s:1, h:1,
          "g+r":1, "d+r":1, "ph+y":1, "r+g":1, "r+ts":1 }
    },

    // prefix : { consonant or stack after : 1 }
    prefixes: {
      g	: { c:1, ny:1, t:1, d:1, n:1, ts:1, zh:1, z:1, y:1, sh:1, s:1 },
      d	: { k:1, g:1, ng:1, p:1, b:1, m:1,
          "k+y":1, "g+y":1, "p+y":1, "b+y":1, "m+y":1,
          "k+r":1, "g+r":1, "p+r":1, "b+r":1 },
      b	: { k:1, g:1, c:1, t:1, d:1, ts:1, zh:1, z:1, sh:1, s:1, r:1, l:1,
          "k+y":1, "g+y":1, "k+r":1, "g+r":1, "r+l":1, "s+l":1,
          "r+k":1, "r+g":1, "r+ng":1, "r+j":1, "r+ny":1, "r+t":1, "r+d":1, "r+n":1, "r+ts":1, "r+dz":1,
          "s+k":1, "s+g":1, "s+ng":1, "s+ny":1, "s+t":1, "s+d":1, "s+n":1, "s+ts":1,
          "r+k+y":1, "r+g+y":1, "s+k+y":1, "s+g+y":1, "s+k+r":1, "s+g+r":1,
          "l+d":1, "l+t":1, "k+l":1, "s+r":1, "z+l":1, "s+w":1 },
      m	: { kh:1, g:1, ng:1, ch:1, j:1, ny:1, th:1, d:1, n:1, tsh:1, dz:1,
          "kh+y":1, "g+y":1, "kh+r":1, "g+r":1 },
     "'"	: { kh:1, g:1, ch:1, j:1, th:1, d:1, ph:1, b:1, tsh:1, dz:1,
         "kh+y":1, "g+y":1, "ph+y":1, "b+y":1, "kh+r":1, "g+r":1, "d+r":1, "ph+r":1, "b+r":1 },

     // typographic quotes
     "\u2018"	: { kh:1, g:1, ch:1, j:1, th:1, d:1, ph:1, b:1, tsh:1, dz:1,
         "kh+y":1, "g+y":1, "ph+y":1, "b+y":1, "kh+r":1, "g+r":1, "d+r":1, "ph+r":1, "b+r":1 },
     "\u2019"	: { kh:1, g:1, ch:1, j:1, th:1, d:1, ph:1, b:1, tsh:1, dz:1,
         "kh+y":1, "g+y":1, "ph+y":1, "b+y":1, "kh+r":1, "g+r":1, "d+r":1, "ph+r":1, "b+r":1 },
     },

    // suffix : 1
    // also included are some Skt letters b/c they occur often in suffix position in Skt words
    suffixes: { "'":1, "\u2018":1, "\u2019":1,
        g:1, ng:1, d:1, n:1, b:1, m:1, r:1, l:1, s:1, N:1, T:1, "-n":1, "-t":1
    },

    // 2nd suffix : letter before
    suff2: {
      s: { g:1, ng:1, b:1, m:1 },
      d: { n:1, r:1, l:1 }
    },

    // root letter index for very ambiguous 3 letter syllables
    ambiguous: {
      dgs	: [ 1, "dgas" ],
     "'gs"	: [ 1, "'gas" ],
      dbs	: [ 1, "dbas" ],
      dms	: [ 1, "dmas" ],
      bgs	: [ 0, "bags" ],
      mngs	: [ 0, "mangs" ],
  }
},


    vars: {},

    init_vars: function() {
        keys = [].concat(
                Object.keys(Wylie.chars.consonant),
                Object.keys(Wylie.chars.subjoined),
                Object.keys(Wylie.chars.vowel),
                Object.keys(Wylie.chars.final),
                Object.keys(Wylie.chars.other),
                ["+"] // this isn't a token in the perl code but I can't see how it can work otherwise
        );

        keys.sort(function(a, b) {
            return (b.length - a.length) || a.localeCompare(b);
        });

        token_choices = keys.map(function(text) {
            return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        }).join("|");

        return {
            token_regexp: new RegExp( token_choices, "g" )
        };
    },

    /* When reverse=false: looking from $i onwards within @tokens, returns as many consonants as it finds,
       up to and not including the next vowel or punctuation.  Skips the caret "^".

       When reverse=true: looking from $i backwards within @tokens, at most up to $orig_i, returns as
       any consonants as it finds, up to and not including the next vowel or
       unctuation.  Skips the caret "^".

       Returns: a string of consonants (in forward order) joined by "+" signs.
    */

    consonant_string: function(tokens, i, reverse, orig_i) {
        j = i;
        step = reverse ? 1 : -1;
        while (j > orig_i && j < tokens.length) {
            if (tokens[j] == "^" || tokens[j] == "+") {
                continue;
            }
            if (!Wylie.chars.consonants[tokens[j]]) {
                break;
            }
            j += step;
        }
        if (j>i) {
            return tokens.slice(i,j).join("+");
        } else {
            return tokens.slice(j,i).join("+");
        }
    },

    decode_one_stack: function(tokens, i) {
        var decoded = "";
        var err = "";
        var carets = 0;
        var consonants = 0;
        var subjoined_consonants = 0;
        var vowel = "";
        var single_consonant = "";
        var final_found = {};
        var vowel_found = "";
        var vowel_sign = "";
        var pluses = 0;

        var orig_i = i;

        if (i+1 < tokens.length && Wylie.sets.superscripts[tokens[i]] && Wylie.sets.superscripts[tokens[i]][tokens[i+1]]) {
            //TODO: checks
            decoded += Wylie.chars.consonant[tokens[i]];
            i++;
            while (tokens[i] == "^") {
                carets++;
                i++;
            }
        }

        while (i < tokens.length) {
            if ((decoded && Wylie.chars.subjoined[tokens[i]]) || Wylie.chars.consonant[tokens[i]]) {
                if (decoded && Wylie.chars.subjoined[tokens[i]]) {
                    decoded += Wylie.chars.subjoined[tokens[i]];
                } else {
                    decoded += Wylie.chars.consonant[tokens[i]];
                }
                i++;

                if (i < tokens.length && tokens[i] == "a") {
                    vowel = "a";
                } else {
                    consonants++;
                    single_consonant = tokens[i];
                }

                //TODO: warn about "dh", etc. (proper EWTS is "d+h")

                while (i < tokens.length && tokens[i] == "^") {
                    carets++;
                    i++;
                }

                // subjoined: rata, yata, lata, wazur.  there can be up two subjoined letters in a stack.
                while (i < tokens.length && Wylie.sets.subscripts[tokens[i]] && subjoined_consonants < 2) {
                    if (consonants > 1 && tokens[i] == "l") {
                        // lata does not occur below multiple consonants (otherwise we mess up "brla" = "b.r+la")
                        break;
                    }
                    //TODO: full stack checking (disabled by "+")

                    decoded += Wylie.chars.subjoined[tokens[i]];
                    subjoined_consonants++;
                    consonants++;
                    i++;

                    while (i < tokens.length && tokens[i] == "^") {
                        carets++;
                        i++;
                    }
                }
            }

            /* caret (^) can come anywhere in Wylie but in Unicode we generate it at the end of
               the stack but before vowels if it came there (seems to be what OpenOffice expects),
               or at the very end of the stack if that's how it was in the Wylie.
            */
            if (carets) {
              //push @warns, "Cannot have more than one \"^\" applied to the same stack." if $caret > 1;
              final_found[ Wylie.sets.final["^"][1] ] = "^";
              decoded += Wylie.sets.final["^"][0];
              carets = 0;
            }

            // vowel(s)
            if (i < tokens.length && Wylie.chars.vowel[tokens[i]]) {
                if (Wylie.chars.complex_vowel[tokens[i]]) {
                    decoded += (decoded ? Wylie.chars.complex_vowel[tokens[i]][1] : Wylie.chars.complex_vowel[tokens[i]][0]);
                } else {
                    if (!decoded) {
                        decoded += Wylie.chars.vowel["a"];
                    }
                    decoded += (tokens[i] == "a" ? "" : Wylie.chars.vowel[tokens[i]]);
                }
                i++;
                vowel_found = tokens[i];
                vowel_sign = (tokens[i] == "a" ? "" : tokens[i]);
            }

            // plus sign: forces more subjoining
            if (i < tokens.length && tokens[i] == "+") {
                i++;
                pluses = 1;

                //TODO: sanity check: next token must be vowel or subjoinable consonant.
                continue;
            }

            // final tokens
            if (i < tokens.length && Wylie.chars.final[tokens[i]]) {
                //TODO: check for duplicates
                final_found[Wylie.chars.final[tokens[i]][1]] = tokens[i];
                decoded += Wylie.chars.final[tokens[i]][0];
                i++;
            }

            single_consonant = "";
            break;
        }

        // if next is a dot "." (stack separator), skip it.
        if (i < tokens.length && tokens[i]==".") {
            i++;
        }

        /* if we had more than a consonant and no vowel, and no explicit "+" joining, backtrack and
           return the 1st consonant alone
        */
        if (consonants > 1 && !vowel_found) {
            if (pluses) {
                //TODO
            } else {
                i = orig_i + 1;
                consonants = 1;
                single_consonant = tokens[i];
                decoded = Wylie.chars.consonant[tokens[i]];
            }
        }

        // single consonant is single consonant
        if (consonants != 1 || pluses) {
            single_consonant = "";
        }

        return {
            err: err,
            decoded: decoded, // converted unicode string
            skipTo: i,
            single_consonant: vowel_found ? null : single_consonant, // single consonant without vowel
            single_a: (vowel_found == "a") ? single_consonant : null,
            visarga: !!final_found[Wylie.chars.final["H"][1]]
        };
    },

    /* the type of token that we are expecting next in the input stream
       - prefix : expect a prefix consonant, or a main stack
       - main   : expect only a main stack
       - suff1  : expect a main stack again, or a 1st suffix
       - suff2  : expect a 2nd suffix
       - none   : expect nothing (after a 2nd suffix)

     valid tsek-bars end in one of these states: suff1, suff2, none
     */

    states: {
        PREFIX: 0,
        MAIN: 1,
        SUFF1: 2,
        SUFF2: 3,
        NONE: 4
    },

    decode_one_tsekbar: function(tokens, i) {
        var state = Wylie.states.PREFIX;
        var decoded = "";
        var visarga = false;
        var err = "";

        while (i < tokens.length && (Wylie.chars.vowel[tokens[i]] || Wylie.chars.consonant[tokens[i]]) && !visarga) {
            var d = Wylie.decode_one_stack(tokens, i);

            if (!d.err) {
                decoded += d.decoded;
                i = d.skipTo;
                visarga = d.visarga;
                continue;
            }

            console.log( "Couldn't decode a stack at "+i+": "+decoded+"?");
            break;

            //TODO: no checking?
        }

        return {
            err: err,
            decoded: decoded,
            skipTo: i
        };
    },

    decode: function(w) {
        var r = "";

        var tokens = w.match(Wylie.vars.token_regexp);
        var i = 0;

        while (i < tokens.length) {
            //TODO: characters to keep untouched
            //TODO: [non-tibetan text] : pass through, nesting brackets
            // punctuation, numbers, etc
            if (Wylie.chars.other[tokens[i]]) {
                r += Wylie.chars.other[tokens[i]];
                i++;
                //TODO: collapse multiple spaces
                continue;
            }

            // vowels & consonants: process tibetan script up to a tsek, punctuation or line noise
            if (Wylie.chars.vowel[tokens[i]] || Wylie.chars.consonant[tokens[i]]) {
                d = Wylie.decode_one_tsekbar(tokens, i);
                if (!d.err) {
                    r += d.decoded;
                    i = d.skipTo;
                    continue;
                }
            }
            // don't know what to do with this
            r += "["+tokens[i]+"]";
            i++;
        }
        return r;
    }

    decode_in_document: function() {
        var nl = document.querySelectorAll("[data-wylie]");
        for (i=0; i<nl.length; i++) {
            var walker = document.createTreeWalker(nl[i], NodeFilter.SHOW_TEXT, null, false);
            while (walker.nextNode()) {
                var n = walker.currentNode;
                n.nodeValue = Wylie.decode(n.nodeValue);
            }
            nl[i].removeAttribute("data-wylie");
            nl[i].setAttribute("lang", "bo");
        }
    }
};

Wylie.vars = Wylie.init_vars();
